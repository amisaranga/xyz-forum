# XYZ Forum

This is the front-end application of the **XYZ Forum** developed using **Vue**
JavaScript framework.

## Project setup

This project requires following software to be installed before setting it up.

- Git
- Node Js 12 or later

### Install

Clone the repository:

```
git clone https://bitbucket.org/amisaranga/xyz-forum.git
```

Move in to the project folder:

```
cd xyz-forum
```

Install packages:

```
npm install
```

### Run

For a test run, enter:

```
npm run serve
```

This will serve the application on `http://localhost:8080`. Enter the URL in a
browser to try out the application.

### Compile and minify for production

For a production ready setup, run:

```
npm run build
```

This will generate the production ready static files in `dist` folder.
This can be served using a webs server.

### Customize configuration

This application connects to an API, whose base URL should be configured in
`src/main.js` file under `Vue.axios.defaults.baseURL`. For a local set up this
is mentioned as `http://localhost:8000/api/v1/`. Please change accordingly if
the URL is different in your setup.
