import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import BootstrapVue from 'bootstrap-vue'
import Snotify from 'vue-snotify'

import auth from '@websanova/vue-auth/dist/v2/vue-auth.common.js'
import driverAuthBearer from '@websanova/vue-auth/dist/drivers/auth/bearer.js'
import driverHttpAxios from '@websanova/vue-auth/dist/drivers/http/axios.1.x.js'
import driverRouterVueRouter from '@websanova/vue-auth/dist/drivers/router/vue-router.2.x.js'

import App from './App.vue'
import router from './router'

Vue.config.productionTip = false

Vue.router = router
Vue.use(Snotify, {
  toast: {timeout: 0, position: 'centerTop'}
})
Vue.use(BootstrapVue)
Vue.use(VueAxios, axios)
Vue.axios.defaults.baseURL = process.env.NODE_ENV === 'production' ? 'https://xyz-forum-api.herokuapp.com/api/v1' : 'http://localhost:8000/api/v1/'
Vue.axios.defaults.headers.common['Content-Type'] = 'application/json'
Vue.axios.defaults.headers.common['Accept'] = 'application/json'
Vue.axios.interceptors.response.use((response) => {
  if (response.status == 401) {
    Vue.auth.logout({redirect: 'Login'})
  }
  return response
}, (error) => {
  // No network, show static page
  if (!error.response) {
    window.location.href = "/service-unavailable.html";
  }

  let path = null;

  switch (error.response.status) {
    case 401: path = '/login'; break;
    case 404: path = '/404'; break;
    case 500: path = '/500'; break;
  }

  if (path != null)
    Vue.router.push(path);
    
  return Promise.reject(error);
})
Vue.use(auth, {
  plugins: {
    http: Vue.axios,
    router: Vue.router
  },
  drivers: {
    auth: driverAuthBearer,
    http: driverHttpAxios,
    router: driverRouterVueRouter
  },
  options: {
    rolesKey: 'type',
  }
})

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import './assets/sass/main.scss'

new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
