import Vue from 'vue'
import Router from 'vue-router'

// Main Layout
const DefaultContainer = () => import('@/containers/DefaultContainer')

// Components
const Posts = () => import('@/components/Posts')
const Post = () => import('@/components/Post')
const PostEditor = () => import('@/components/PostEditor')

// Pages
// Views - Pages
const Page404 = () => import('@/views/pages/Page404')
const Page500 = () => import('@/views/pages/Page500')
const Login = () => import('@/views/pages/Login')
const Register = () => import('@/views/pages/Register')

Vue.use(Router)

export default new Router({
  mode: 'history', // https://router.vuejs.org/api/#mode
  linkActiveClass: 'open active',
  scrollBehavior: () => ({ y: 0 }),
  routes: [
    {
      path: '/',
      redirect: '/posts',
      name: 'Home',
      component: DefaultContainer,
      children: [
        {
          path: 'posts',
          name: 'Posts',
          component: Posts,
          meta: {
            auth: true
          }
        },
        {
          path: 'posts/pending',
          name: 'Pending Posts',
          component: Posts,
          meta: {
            auth: true
          },
          props: {
            mode: 'admin'
          }
        },
        {
          path: 'posts/new',
          name: 'New Post',
          component: PostEditor,
          meta: {
            auth: true
          }
        },
        {
          path: 'posts/:id',
          name: 'Post',
          component: Post,
          meta: {
            auth: true
          }
        },
        {
          path: 'posts/:id/edit',
          name: 'Edit Post',
          component: PostEditor,
          meta: {
            auth: true
          }
        }
      ]
    },
    {
      path: '/',
      redirect: '/404',
      name: 'Pages',
      component: {
        render (c) { return c('router-view') }
      },
      children: [
          {
            path: '404',
            name: 'Page404',
            component: Page404
          },
          {
            path: '500',
            name: 'Page500',
            component: Page500
          },
          {
            path: 'login',
            name: 'Login',
            component: Login
          },
          {
            path: 'register',
            name: 'Register',
            component: Register
          }
      ]
    },
    {
        path: '*',
        component: Page404,
    }
  ]
})
